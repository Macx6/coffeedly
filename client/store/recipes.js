import {
  ADD_LIKE_ACTION,
  CREATE_RECIPE_ACTION,
  GET_RECIPES_ACTION,
  REMOVE_LIKE_ACTION
} from '~/action-types'
import {
  FIREBASE_COLLECTION_RECIPES,
  FIREBASE_COLLECTION_USERS
} from '~/const-types'
import {
  ADD_RECIPE_MUTATION,
  SET_RECIPES_MUTATION,
  UPDATE_RECIPE_LIKE_COUNT
} from '~/mutation-types'

export const state = () => ({
  recipes: [],
  lastDoc: null,
  loading: false
})

export const mutations = {
  [ADD_RECIPE_MUTATION](state, payload) {
    state.recipes.unshift(payload)
  },
  [SET_RECIPES_MUTATION](state, list) {
    state.recipes = list
  },
  [UPDATE_RECIPE_LIKE_COUNT](state, { recipe, value }) {
    const item = state.recipes.findIndex((item) => item.id === recipe.id)
    state.recipes[item].likedByUser = value > 0
    state.recipes[item].likes += value
  },
  _setLastDoc(state, doc) {
    state.lastDoc = doc
  }
}

export const actions = {
  async [CREATE_RECIPE_ACTION]({ commit, dispatch, state }, payload) {
    try {
      const docRef = await this.$fireService.addDocumentToCollection({
        collection: FIREBASE_COLLECTION_RECIPES,
        payload
      })
      const user = await this.$fireService.getDocumentFromCollection({
        collection: FIREBASE_COLLECTION_USERS,
        document: { uid: payload.user_uid }
      })
      const doc = {
        id: docRef.id,
        likes: 0,
        likedByUSer: false,
        user,
        ...payload
      }
      commit(ADD_RECIPE_MUTATION, doc)
    } catch {}
  },

  async [GET_RECIPES_ACTION]({ commit, dispatch, state }, { loader, user }) {
    try {
      if (state.loading) {
        return
      }
      state.loading = true
      await this.$fireService
        .getDocumentsFromCollection({
          collection: FIREBASE_COLLECTION_RECIPES,
          user,
          lastDoc: state.lastDoc
        })
        .then(({ results, lastDoc }) => {
          if (results.length <= 0) {
            loader.complete()
          }
          commit('_setLastDoc', lastDoc)
          commit(SET_RECIPES_MUTATION, results)
        })

      loader.loaded()
      state.loading = false
    } catch (e) {
      console.log(e)
    }
  },
  async [ADD_LIKE_ACTION]({ commit }, { recipe, user }) {
    try {
      await this.$fireStore
        .collection(FIREBASE_COLLECTION_RECIPES)
        .doc(recipe.id)
        .collection('likedBy')
        .doc(user.uid)
        .set({ like: true })
        .then(() => commit(UPDATE_RECIPE_LIKE_COUNT, { recipe, value: 1 }))
    } catch (e) {
      console.log(e)
    }
  },
  async [REMOVE_LIKE_ACTION]({ commit }, { recipe, user }) {
    try {
      await this.$fireStore
        .collection(FIREBASE_COLLECTION_RECIPES)
        .doc(recipe.id)
        .collection('likedBy')
        .doc(user.uid)
        .delete()
        .then(() => commit(UPDATE_RECIPE_LIKE_COUNT, { recipe, value: -1 }))
    } catch (e) {
      console.log(e)
    }
  }
}
